﻿using System;

namespace DefaultLiteral
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            //previous
//            bool isWorking = false;
//            string someRandomVar = String.Empty;
            
            var isWorking = default(bool);
            var someRandomVar = default(string);

            if (true)//some expression
            {
                isWorking = true;
                someRandomVar = "Just some test value";
            }
            

        }
    }
}