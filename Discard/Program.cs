﻿using System;

namespace Discard
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter some text so i can specify the type");
            var input = Console.ReadLine();
            if (int.TryParse(input, out _))
            {
                Console.WriteLine("Numeric value entered");
            }
            else if (bool.TryParse(input, out _))
            {
                Console.WriteLine("Boolean value entered");
            }
            else
            {
                Console.WriteLine("String value entered");
            }
        }
    }
}