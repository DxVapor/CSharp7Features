﻿using System;

namespace OutVariables
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("enter your operation");
            var operation = Console.ReadLine()?.Trim();
            
            Console.WriteLine("please enter variable 1");
            if (float.TryParse(Console.ReadLine(), out var variable1))
            {
                
                Console.WriteLine("please enter variable 2");
                if (float.TryParse(Console.ReadLine(), out var variable2))
                {
                    Console.WriteLine($"Now performing {variable1} {operation} {variable2}");
                    
                    float result = 0;
                    switch (operation)
                    {
                        case "+":
                            result = variable1 + variable2;
                            break;
                        case "/":
                            result = variable1 / variable2;
                            break;
                        case "-":
                            result = variable1 - variable2;
                            break;
                        case "*":
                            result = variable1 * variable2;
                            break;
                        case "%":
                            result = variable1 % variable2;
                            break;
                    }

                    Console.WriteLine($"Result: {result}");
                }
            }
        }
    }
}