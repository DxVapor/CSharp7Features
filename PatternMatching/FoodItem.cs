﻿namespace PatternMatching
{
    public abstract class FoodItem
    {
        public string Variant { get; }

        protected FoodItem(string variant)
        {
            Variant = variant;
        }

        public override string ToString()
        {
            return $"{Variant} {GetType().Name}";
        }
    }
}