﻿namespace PatternMatching
{
    public class Person
    {
        public string GetEatingUtensils(FoodItem foodItem)
        {
            switch (foodItem)
            {
                case null:
                    return "Specify a food atleast";
                case Pizza _:
                    return "Hands";
                case Curry _:
                    return "Fork";
                default:
                    return "What food?";
            }
        }
    }
}