﻿using System;

namespace PatternMatching
{
    internal class Program
    {
        public static void Main(string[] args)
        {
//            var foodItem = new Pizza("Pinapple");
            var foodItem = new Curry("Mild");
            
            var me = new Person();
            Console.WriteLine( foodItem ?? throw new Exception("asdasdasd"));
            Console.WriteLine($"I will consume my {foodItem} using {me?.GetEatingUtensils(foodItem)}");
        }
    }
}